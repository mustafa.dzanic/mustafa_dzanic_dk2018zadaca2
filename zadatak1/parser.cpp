#include <iostream>
#include <string>
#include <vector>
#include "token.hpp"
#include "parser.h"

using namespace std;


Parser::Parser(std::vector<Token> const & t) 
  : tokens(t), bufferLength(t.size())
{}

bool Parser::parse(){
  currentIndex = 0;
  bool retval = Start();

  if(currentIndex < bufferLength) retval = false;
  
  return retval;
}

bool Parser::epsilon(){
  return true;  
}

bool Parser::terminal(int t){
  if(currentIndex >= bufferLength) return false;
  bool x=  (t == tokens[currentIndex].tag);
  if (x == true)
   currentIndex++;
  return x;
}

bool Parser::Start(){
  return tag() && tagContent() && sPrime();
}

bool Parser::sPrime(){
  return ( Start() && sPrime() ) || epsilon();
}

bool Parser::tagContent(){
  return (terminal(TAGEND) && contents() && terminal(TAGCLOSE)) || (terminal(TAGENDANDCLOSE) && contents());
}

bool Parser::tag(){
  return terminal(TAGBEGIN) && attr();
}

bool Parser::attr(){
  return (terminal(ATTRIBUTENAME) && terminal(EQUAL) && terminal(ATTRIBUTEVALUE) && attr()) || epsilon();
}

bool Parser::contents(){
  return terminal(CONTENT) || Start() || epsilon(); 
}

