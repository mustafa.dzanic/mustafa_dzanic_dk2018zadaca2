
%{
#include "token.hpp"
int lineCount=1, columnCount=1;
int sum[9] = {0};
%}
ws            [ \t]
letter        [a-zA-Z]
digit         [0-9]
number        {digit}+
identifier    {letter}({letter}|{digit})*

%s OUTSIDETAG
%s INSIDETAG
%%

{ws}                {columnCount+=yyleng;}
"\n"                {lineCount++; columnCount=1;}
"<"{identifier}     {sum[TAGBEGIN]++; BEGIN(INSIDETAG);return TAGBEGIN;}
"</"{identifier}">"    {sum[TAGCLOSE]++; return TAGCLOSE;}
">"                 {sum[TAGEND]++; BEGIN(OUTSIDETAG);return TAGEND;}
"\"".*"\""          {sum[ATTRIBUTEVALUE]++; return ATTRIBUTEVALUE;}
"/>"                {sum[TAGENDANDCLOSE]++; BEGIN(OUTSIDETAG);return TAGENDANDCLOSE;}
<INSIDETAG>{identifier}        {sum[ATTRIBUTENAME]++; return ATTRIBUTENAME;}
"="                 {sum[EQUAL]++; return EQUAL;}
<OUTSIDETAG>[^<\n]* {sum[CONTENT]++; return CONTENT;}




%%

// Token getToken(){
//     int t = yylex();
//     Token tok = Token(t, yytext);
//     if(t == ATTRIBUTEVALUE){
//         tok.lexeme = std::string(tok.lexeme, 1, yyleng-2);
//     }
//     return tok;
// }

// int main(void){
    
//   Token t;

//   while((t = getToken()).tag != 0){
//     std::cout<<t.info()<<" line "<<lineCount<<", column "<<(t.tag==ATTRIBUTEVALUE ? columnCount+1 : columnCount)<<std::endl;
//     columnCount+=yyleng;
//   } 
//   std::cout<<"TAGBEGIN: "<<sum[TAGBEGIN]<<"\nTAGEND: "<<sum[TAGEND]<<"\nTAGCLOSE: "<<sum[TAGCLOSE]<<"\nTAGENDANDCLOSE: "<<sum[TAGENDANDCLOSE]
//           <<"\nATTRIBUTENAME: "<<sum[ATTRIBUTENAME]<<"\nEQUAL: "<<sum[EQUAL]<<"\nATTRIBUTEVALUE: "<<sum[ATTRIBUTEVALUE]<<"\nCONTENT: "<<sum[CONTENT];
//   return 0;
// }

int yywrap(){
  return 1;
}
