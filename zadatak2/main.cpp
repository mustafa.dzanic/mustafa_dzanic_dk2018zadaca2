#include "Parser.hpp"

extern "C" int yylex();
extern "C" char *yytext;
extern int line_num;

using namespace std;

int main(int argc, char const *argv[])
{
  int tag;
  vector<Token> tokens; 
  while (tag = yylex())
    tokens.emplace_back(tag, yytext, line_num);
  
  Parser parser{tokens};
  parser.parse();

  cout << "Input fajl validan." << endl;

  return 0;
}
