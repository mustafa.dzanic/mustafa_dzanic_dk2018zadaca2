
%{
#include "xml.tab.h"
#define YY_DECL extern "C" int yylex()
int line_num = 1;
%}

ws        [ \t]+
letter    [a-zA-Z]
digit     [0-9]
name      {letter}+
number    {digit}+
attribute {letter}({letter}|{digit})*

%x INSIDETAG

%%

<INSIDETAG>{attribute} { return ATTRIBUTENAME; }

<INITIAL,INSIDETAG>{

{ws}          ;
"<"{name}     { BEGIN(INSIDETAG); return TAGBEGIN;       }
">"           { BEGIN(INITIAL);   return TAGEND;         }
"</"{name}">" { return TAGCLOSE;                         }
"/>"          { BEGIN(INITIAL);   return TAGENDANDCLOSE; }
"="           { return EQUAL;                            }
\"[^\"]*\"    { return ATTRIBUTEVALUE;                   }
\n            { ++line_num;                              }
.             ;

}

<INITIAL>[^<>\n]+ { return CONTENT; }

%%